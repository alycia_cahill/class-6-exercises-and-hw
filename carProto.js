
const Car = function (model) {
    this.model= model; 
    this.speed = 0; 
}; 

Car.prototype.accelerate = function (){
    this.speed++; 
}; 

Car.prototype.brake = function (){
    this.speed--; 
}; 

Car.prototype.toString = function (){
    return `This ${this.model} car is traveling at this ${this.speed} mph`; 
}; 


const newCar = new Car('Taurus'); 
newCar.accelerate(); 
newCar.accelerate(); 
newCar.brake(); 
console.log(newCar.toString()); 
