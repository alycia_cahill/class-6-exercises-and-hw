// Attach one listener that will detect clicks on any of the <td>
// elements.  Should update that element's innerHTML to be the
// x, y coordinates of the mouse at the time of the click
let cell = document.querySelectorAll('td');

for(i=0; i < cell.length; i++){
    cell[i].addEventListener('click', function(e){
        let cell = e.target; 
        cell.innerHTML = `${e.clientX}, ${e.clientY}`; 
    });  
}; 


