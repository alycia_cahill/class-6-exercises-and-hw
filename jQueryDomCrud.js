
$( document ).ready(function(){

	// Create a new <a> element containing the text "Buy Now!" 
	// with an id of "cta" after the last <p>
	let cta = $("<a id='cta'>Buy Now!</a>"); 
	$("main").append(cta); 


	// Access (read) the data-color attribute of the <img>,
	// log to the console
	const $img = $('img'); 
	const $imgColor =$img.data("color"); 
	console.log($imgColor); 

	// Update the third <li> item ("Turbocharged"), 
	// set the class name to "highlight"
	$("ul li:nth-child(3)").addClass('highlight'); 


	// Remove (delete) the last paragraph
	// (starts with "Available for purchase now…"
	$('p').remove(); 
	
}); 