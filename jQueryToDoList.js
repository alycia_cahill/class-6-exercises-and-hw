$(document).ready(function(){
  // If an li element is clicked, toggle the class "done" on the <li>
  $('li').click(done); 

    function done(){
      $(this).toggleClass('done'); 
    };
  // If a delete link is clicked, delete the li element / remove from the DOM
   $('a.delete').click(btnDelete); 

    function btnDelete(){
      event.stopImmediatePropagation(); 
      $(this).parent().fadeOut("slow", function(){
          $(this).remove(); 
      }); 
    }; 


  // If a "Move to..."" link is clicked, it should move the item to the correct
  // list.  Should also update the working (i.e. from Move to Later to Move to Today)
  // and should update the class for that link.
  // Should *NOT* change the done class on the <li>
    $('a.toLater').click(later); 
    
    function later(){
      event.stopImmediatePropagation(); 
      $(this).parent().appendTo('ul.later-list'); 
      $(this).removeClass('toLater').addClass('toToday').text('Move to Today'); 
      $(this).click(today); 
    }; 
     
    $('a.toToday').click(today); 

    function today(){
      event.stopImmediatePropagation(); 
      $(this).parent().appendTo('ul.today-list'); 
      $(this).removeClass('toToday').addClass('toLater').text('Move to Later'); 
      $(this).click(later); 
    }; 



  // If an 'Add' link is clicked, adds the item as a new list item in correct list
  // addListItem function has been started to help you get going!  
  // Make sure to add an event listener to your new <li> (if needed)
  
  function addListItem (e) {
    e.preventDefault();
    const text = $(this).parent().find('input').val();
    const list = $(this).parent().prev('ul'); 
    const newLi = $('<li>'); 
    const newDelete = $('<a class="delete"></a>').text('Delete'); 
    const newMove = $('<a class="move"></a>'); 
    $(newDelete).prependTo(newLi); 
    $(newMove).prependTo(newLi); 
    const newSpan = $('<span>').text(text); 
    $(newSpan).prependTo(newLi); 

    if ( $(list).hasClass("today-list")){
      newMove.addClass('toLater').text('Move to Later'); 
    }else{
      newMove.addClass('toToday').text('Move to Today'); 
    }

    const listItem = $(list).append(newLi); 
    $(this).parent().find('input').val('');
    $('li').click(done); 
    $('a.delete').click(btnDelete); 
    $('a.toToday').click(today); 
    $('a.toLater').click(later); 
  }
  // Add this as a listener to the two Add links
  $('a.add-item').click(addListItem); 

}); 